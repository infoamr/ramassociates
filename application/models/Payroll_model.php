<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Payroll_model extends App_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Get client object based on passed clientid if not passed clientid return array of all clients
     * @param  mixed $id    client id
     * @param  array  $where
     * @return mixed
     */

    public function get_all_payroll()
    {
        // $this->db->select('payrolls.*, clients.company, staff.firstname,staff.lastname');
        $this->db->select('payrolls.*, clients.company,federal_forms.form_name');
        $this->db->from(db_prefix() . 'payrolls');
        $this->db->join(db_prefix() . 'clients', 'clients.userid = payrolls.client_id');
        $this->db->join(db_prefix() . 'staff', 'staff.staffid = payrolls.assigned_to');
        $this->db->join(db_prefix() . 'federal_forms', 'federal_forms.id = payrolls.federal_form');
        $this->db->order_by('dateadded', 'DESC');
        $query = $this->db->get()->result_array();
        // echo "<pre>";print_r($query);echo "</pre>"; die;
        return $query;
    }

    public function get_payroll_byid($id)
    {
        $this->db->select('payrolls.*, clients.company,staff.firstname,staff.lastname,federal_forms.form_name');
        $this->db->where('payrolls.id',$id);
        $this->db->from(db_prefix() . 'payrolls');
        $this->db->join(db_prefix() . 'clients', 'clients.userid = payrolls.client_id');
        $this->db->join(db_prefix() . 'staff', 'staff.staffid = payrolls.assigned_to');
        $this->db->join(db_prefix() . 'federal_forms', 'federal_forms.id = payrolls.federal_form');
        return $this->db->get()->result_array();
    }

    public function get_followers_by_payrollid($id)
    {
        $this->db->select('payroll_followers.*,staff.firstname,staff.lastname');
        $this->db->where('payroll_id',$id);
        $this->db->from(db_prefix() . 'payroll_followers');
        $this->db->join(db_prefix() . 'staff', 'staff.staffid = payroll_followers.staffid');
        return $this->db->get()->result_array();
    }

    public function get_states()
    {
        $this->db->from(db_prefix() . 'state');
        return $this->db->get()->result_array();
    }
    
    public function get_reminder_by_payrollid($id)
    {
        $this->db->where('payroll_id',$id);
        $this->db->from(db_prefix() . 'payroll_reminders');
        return $this->db->get()->result_array();
    }

    public function get_states_by_payrollid($id)
    {
        $this->db->where('payroll_id',$id);
        $this->db->from(db_prefix() . 'payroll_states');
        return $this->db->get()->result_array();
    }

    /**
     * Delete payroll and all connections
     * @param  mixed $id taskid
     * @return boolean
     */
    public function delete_payroll($id)
    {
        $this->db->where('id', $id);
        $this->db->delete(db_prefix() . 'payrolls');

        $this->db->where('payroll_id', $id);
        $this->db->delete(db_prefix() . 'payroll_reminders');

        $this->db->where('payroll_id', $id);
        $this->db->delete(db_prefix() . 'payroll_followers');

        $this->db->where('payroll_id', $id);
        $this->db->delete(db_prefix() . 'payroll_states');

        log_activity('Payroll Deleted [ID:'.$id.']');
        return true;        
    }

    public function delete_paystate($id)
    {
        $this->db->where('id', $id);
        $this->db->delete(db_prefix() . 'payroll_states');
        return true;
    }

    public function search_pay_report($data)
    {
        // echo '<pre>'; print_r($data); echo '</pre>'; exit;
        if(!empty($data['from']))
        {
            $data['from'] = date('Y-m-d', strtotime($data['from']));
            $this->db->where('dateadded >=', $data['from']);
        }
        if(!empty($data['to']))
        {
            $data['to'] = date('Y-m-d', strtotime($data['to']));
            $this->db->where('dateadded <=', $data['to']);
        }
        if(!empty($data['assigned_to']))
        {
            $this->db->where('assigned_to', $data['assigned_to']);
        }
        if(!empty($data['client_id']))
        {
            $this->db->where('client_id', $data['client_id']);
        }
        
        $this->db->select('payrolls.*, clients.company,staff.firstname,staff.lastname');
        $this->db->from(db_prefix() . 'payrolls');
        $this->db->join(db_prefix() . 'clients', 'clients.userid = payrolls.client_id');
        $this->db->join(db_prefix() . 'staff', 'staff.staffid = payrolls.assigned_to');
        return $this->db->get()->result_array();
    }

    public function get_federal_forms()
    {
        $this->db->from(db_prefix() . 'federal_forms');
        return $this->db->get()->result_array();
    }

    public function get_state_forms()
    {
        $this->db->from(db_prefix() . 'state_forms');
        return $this->db->get()->result_array();
    }

    // public function get_federals_by_payrollid($id)
    // {
    //     $this->db->select('federal_forms.form_name');
    //     $this->db->where('payroll_id',$id);
    //     $this->db->from(db_prefix() . 'payroll_federals');
    //     $this->db->join(db_prefix() . 'federal_forms', 'federal_forms.id = payroll_federals.formid');
    //     return $this->db->get()->result_array();
    // }
    
}
