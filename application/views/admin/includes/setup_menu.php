<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<div id="setup-menu-wrapper" style="width: 100%!important; min-height: 100px!important" class="animated <?php if($this->session->has_userdata('setup-menu-open')
    && $this->session->userdata('setup-menu-open') == true){echo 'display-block';} ?>">
    <div class="container" style="width: 100%">
    <ul class="nav navbar-nav" id="setup-menu">
        <li>
            <a class="close-customizer"><i class="fa fa-close"></i></a>
        </li>
        <?php
        $totalSetupMenuItems = 0;
        foreach($setup_menu as $key => $item){
         if(isset($item['collapse']) && count($item['children']) === 0) {
           continue;
       }
       $totalSetupMenuItems++;
       ?>
       <li class="custom_setup" class="dropdown customers-nav-item-<?php echo $item['slug']; ?> ">
         <a href="<?php echo count($item['children']) > 0 ? '#' : $item['href']; ?>" aria-expanded="false">
             <i class="<?php echo $item['icon']; ?> typcn typcn-book"></i>
             <span class="menu-text">
                 <?php echo _l($item['name'],'', false); ?>
             </span>
             <?php if(count($item['children']) > 0){ ?>
                 <i class="caret"></i>
             <?php } ?>
         </a>
         <?php if(count($item['children']) > 0){ ?>
             <ul class="dropdown submenu-item" aria-expanded="true">
                <?php foreach($item['children'] as $submenu){
                   ?>
                   <li class="customers-nav-item-<?php echo $submenu['slug']; ?>">
                    <a aria-haspopup="true" href="<?php echo $submenu['href']; ?>" class="nav-link">
                       <?php if(!empty($submenu['icon'])){ ?>
                           <i class="<?php echo $submenu['icon']; ?> menu-icon"></i>
                       <?php } ?>
                       <span class="sub-menu-text">
                          <?php echo _l($submenu['name'],'',false); ?>
                      </span>
                  </a>
              </li>
          <?php } ?>
      </ul>
  <?php } ?>
</li>
<?php hooks()->do_action('after_render_single_setup_menu', $item); ?>
<?php } ?>

<?php //if(get_option('show_help_on_setup_menu') == 1 && is_admin()){ $totalSetupMenuItems++; ?>
    <!-- <li>
        <a href="<?php //echo hooks()->apply_filters('help_menu_item_link','https://help.perfexcrm.com'); ?>" target="_blank">
            <?php //echo hooks()->apply_filters('help_menu_item_text',_l('setup_help')); ?>
        </a>
    </li> -->
<?php //} ?>
</ul>
</div>
</div>
<?php $this->app->set_setup_menu_visibility($totalSetupMenuItems); ?>



<script>
$(document).ready(function(){
  $("button").click(function(){
    $("p").toggle();
  });
});
</script>
