<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>View Payroll</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
<style type="text/css">
      .clearfix:after {
  content: "";
  display: table;
  clear: both;
}

a {
  color: #5D6975;
  text-decoration: underline;
}

body {
  position: relative;
  width: 21cm;  
  height: 29.7cm; 
  margin: 0 auto; 
  color: #001028;
  background: #FFFFFF; 
  font-family: Arial, sans-serif; 
  font-size: 12px; 
  font-family: Arial;
}

header {
  padding: 10px 0;
  margin-bottom: 30px;
}

#logo {
  text-align: center;
  margin-bottom: 10px;
}

#logo img {
  width: 90px;
}

h4 {
  /*border-top: 1px solid  #5D6975;*/
  border-bottom: 1px solid  #5D6975;
  color: #5D6975;
  font-size: 2.4em;
  line-height: 1.4em;
  font-weight: normal;
  text-align: center;
  margin: 0 0 20px 0;
  background: url(dimension.png);
}

#project {
  float: left;
}

#project span {
  color: #000000;
  text-align: left;
  width: 92px;
  margin-right: 10px;
  display: inline-block;
  font-size: 1.2em;
  font-weight: bold;
  margin-bottom: 5px;
}

#project P {
  color: #000000;
  text-align: left;
  width: 92px;
  margin-right: 10px;
  display: inline-block;
  font-size: 1.2em;
  font-weight: normal;
  margin:0px;
  padding: 0px; 
  margin-bottom: 5px;
}

#company {
  float: right;
  text-align: right;
}

#project div,
#company div {
  white-space: nowrap;        
}

table {
  width: 100%;
  border-collapse: collapse;
  border-spacing: 0;
  margin-bottom: 20px;
}

table tr:nth-child(2n-1) td {
  background: #F5F5F5;
}

table th,
table td {
  text-align: center;
}

table th {
  padding: 5px 20px;
  color: #5D6975;
  border-bottom: 1px solid #C1CED9;
  white-space: nowrap;        
  font-weight: normal;
}

table .service,
table .desc {
  text-align: left;
}

table td {
  padding: 20px;
  text-align: center;
}

table td.service,
table td.desc {
  vertical-align: top;
}

table td.unit,
table td.qty,
table td.total {
  font-size: 1.2em;
}

table td.grand {
  border-top: 1px solid #5D6975;;
}

#notices .notice {
  color: #5D6975;
  font-size: 1.2em;
}

footer {
  color: #5D6975;
  width: 100%;
  height: 30px;
  position: absolute;
  bottom: 0;
  border-top: 1px solid #C1CED9;
  padding: 8px 0;
  text-align: center;
}
  </style>
  </head>
  <body>
    <header class="clearfix">
      <!-- <div id="logo">
        <img src="logo.png">
      </div> -->
      <h4>Payroll Detail(<?php echo $fetch_data[0]['payroll_number'] ?>)</h4>
      <div id="company" class="clearfix">
        <div>Company Name</div>
        <div>455 Foggy Heights,<br /> AZ 85004, US</div>
        <div>(602) 519-0450</div>
        <div><a href="mailto:company@example.com">company@example.com</a></div>
      </div>
      <div id="project">
        <div style="width: 50%; float: left">
        <div><span>Company</span><p> <?php echo $fetch_data[0]['company'] ?></p></div>
        <div><span>Federal Form</span><p><?php echo $fetch_data[0]['form_name'] ?></p></div>
        <div><span>Form Status</span>
          <?php 
            if($fetch_data[0]['federal_status'] == 1) {
              $fedstatus = 'Pending';
            }
            if($fetch_data[0]['federal_status'] == 2) {
              $fedstatus = 'In Progress';
            }
            if($fetch_data[0]['federal_status'] == 3) {
              $fedstatus = 'Filed';
            } ?>
         <p> <?php echo $fedstatus; ?> </p>
        </div>
      </div>

      <div style="width: 50%; float: right; ">
        <div><span>Assigned To</span>
          <p><?php echo $fetch_data[0]['firstname'].' '.$fetch_data[0]['lastname'] ?></p>
        </div>
        <div><span>Priority</span>
          <?php 
            if($fetch_data[0]['priority'] == 1) {
              $priority = 'Low';
            }
            if($fetch_data[0]['priority'] == 2) {
              $priority = 'Medium';
            }
            if($fetch_data[0]['priority'] == 3) {
              $priority = 'High';
            }
            if($fetch_data[0]['priority'] == 4) {
              $priority = 'Urgent';
            } ?>
            <p><?php echo $priority; ?></p>
        </div>
        <div><span>Start Date</span><p><?php $stdate = date("d F Y", strtotime($fetch_data[0]['startdate'])); echo $stdate; ?></p></div>
        <div><span>Due Date</span><p><?php $ddate = date("d F Y", strtotime($fetch_data[0]['duedate'])); echo $ddate; ?></p></div>
      </div>
      </div>
    </header>
    <main>
      <table class="table table-bordered" width="100%" style="font-size:14px">
        <thead>
          <tr style="background-color:#2e4056;color:#ffffff;font-family: Roboto, 'Segoe UI', Tahoma, sans-serif;">
            <th class="col-md-4 text-center"><h5 style="font-weight: bold; color:#03A9F4;">State</h5></th>
            <th class="col-md-8 text-center"><h5 style="font-weight: bold; color:#03A9F4;">Forms / Status</h5></th>
          </tr>
        </thead>
        <tbody id="tbody">
        <?php 
          foreach($states as $als) { ?>
          <tr>
            <td class="text-center"><b><?php echo $als['state']; ?></b></td>
            <?php $formsdata = json_decode($als['forms_with_status'], true);
              $statesdata = '';
              foreach($formsdata as $fms) {
                $fmstatus = ($fms['status']==1) ? "<span class='text-warning'>Pending</span>" : (($fms['status']==2)  ? "<span class='text-info'>In Progress</span>" : "<span class='text-success'>Filed</span>");
                $CI = &get_instance();
                $sttype = $CI->db->select('state_type')->where('id', $fms['id'])->get(db_prefix() . 'state_forms')->result_array();
                $statesdata .= '<b>'.$sttype[0]['state_type'].' - '.$fmstatus.'</b><br/>'; 
              } ?>
            <td class="text-center"><?php echo $statesdata; ?></td>
          </tr>
        <?php } ?>
        </tbody>
      </table>
    </main>
    <header class="clearfix">
      <div id="project">
        <div style="width: 50%; float: left; ">
          <div><span>Reminder Date</span><p><?php $rmd = date("d F Y", strtotime($reminder[0]['date'])); echo $rmd; ?></p>
          </div>
          <div><span>Description</span><p><?php echo $reminder[0]['description'] ?></p></div>
          <div><span>Followers</span><br/>  
          <?php if(!empty($followers)) { 
            foreach($followers as $data) { 
               echo $data['firstname'].' '.$data['lastname'].', ';
              }
            } 
            else { ?>
            <p style="font-weight: normal; font-size: 15px">No Followers</p>
          <?php } ?>
          </div>
          <div><span>Remarks</span><p><?php echo $fetch_data[0]['remarks'] ?></p></div>
        </div>

        <div style="width: 50%; float: right; ">
          <div><span>Reminder to</span><p><?php echo $reminder[0]['email'] ?></p></div>
        </div>
      </div>
    </header>
    
    <footer>
    </footer>
  </body>
</html>