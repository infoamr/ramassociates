<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<?php init_head(); ?>
<div id="wrapper">
   <div class="content">
      <div class="row">
         <div class="col-md-12">
            <div class="panel_s">
               <div class="panel-body">
                  <div class="clearfix"></div>
                  <div class="row">
                    <div class="col-md-12">
                    <?php echo form_open(admin_url('tasks/search_worksreport')); ?>
                     <div class="col-md-2">
                        <?php echo render_date_input('from','','From'); ?>
                     </div>
                     <div class="col-md-2">
                        <?php echo render_date_input('to','','To'); ?>
                     </div>

                     
                     <!-- <div class="col-md-2">
                        <div class="select-placeholder">
                           <select name="assigned_to" id="assigned_to" class="selectpicker" data-live-search="true" data-width="100%">
                              <option value=""><?php //echo _l('all_staff_members'); ?></option>
                              <?php //foreach($staff as $st){ ?>
                              <option value="<?php //echo $st['staffid']; ?>"><?php //echo $st['full_name']; ?></option>
                              <?php // } ?>
                           </select>
                        </div>
                     </div> -->
                     

                      <div class="col-md-3">
                        <div class="select-placeholder">
                        <!-- <label for="rel_type" class="control-label">Work Type</label> -->
                          <select name="work_type" class="selectpicker" id="work_type" data-live-search="true" data-width="100%" data-none-selected-text="<?php echo _l('dropdown_non_selected_tex'); ?>">
                             <option value="">All Work Type</option>
                             <?php foreach($work_type_data->result() as $work_type) { ?>
                             <option value="<?php echo $work_type->work_type?>" <?php if(isset($task) && $task->work_type == $work_type->work_type){echo 'selected';} ?>><?php echo $work_type->work_type?></option>
                             <?php } ?>
                          </select>
                        </div>
                      </div>

                     <div class="col-md-3">
                        <div class="select-placeholder">
                           <select id="statuses" name="statuses" data-live-search="true" data-width="100%" class="selectpicker">
                            <option value="">All Statuses</option>
                            <option value="1"><?php echo _l('task_status_1'); ?></option>
                            <option value="2"><?php echo _l('task_status_2'); ?></option>
                            <option value="3"><?php echo _l('task_status_3'); ?></option>
                            <option value="4"><?php echo _l('task_status_4'); ?></option>
                            <option value="5"><?php echo _l('task_status_5'); ?></option>
                            <option value="6"><?php echo _l('task_status_6'); ?></option>
                            <option value="7"><?php echo _l('task_status_7'); ?></option>
                            <option value="8"><?php echo _l('task_status_8'); ?></option>
                            <option value="9"><?php echo _l('task_status_9'); ?></option>
                            <option value="10"><?php echo _l('task_status_10'); ?></option>
                            <option value="11"><?php echo _l('task_status_11'); ?></option>
                            <option value="12"><?php echo _l('task_status_12'); ?></option>
                            <option value="13"><?php echo _l('task_status_13'); ?></option>
                            <option value="14"><?php echo _l('task_status_14'); ?></option>
                            <option value="15"><?php echo _l('task_status_15'); ?></option>
                           </select>
                        </div>
                     </div>
                     
                     <div class="col-md-2">
                      <button type="submit" class="btn btn-info pull-left"><?php echo _l('apply'); ?></button>
                     </div>
                    <?php echo form_close(); ?>
                    </div>

                    <div class="col-md-12">
                      <hr class="no-mtop"/>
                    </div>

                  </div>
                  <div class="clearfix"></div>
                  <?php $this->load->view('admin/tasks/_table',array('bulk_actions'=>true)); ?>
                  <?php $this->load->view('admin/tasks/_bulk_actions'); ?>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<?php init_tail(); ?>
<script>
   var staff_member_select = $('select[name="staff_id"]');
   $(function() {

    init_ajax_projects_search();
    var ctx = document.getElementById("timesheetsChart");
    var chartOptions = {
      type: 'bar',
      data: {
        labels: [],
        datasets: [{
          label: '',
          data: [],
          backgroundColor: [],
          borderColor: [],
          borderWidth: 1
        }]
      },
      options: {
        responsive: true,
        maintainAspectRatio: false,
        tooltips: {
          enabled: true,
          mode: 'single',
          callbacks: {
            label: function(tooltipItems, data) {
              return decimalToHM(tooltipItems.yLabel);
            }
          }
        },
        scales: {
          yAxes: [{
            ticks: {
              beginAtZero: true,
              min: 0,
              userCallback: function(label, index, labels) {
                return decimalToHM(label);
              },
            }
          }]
        },
      }
    };

    var timesheetsTable = $('.table-timesheets-report');
    $('#apply_filters_timesheets').on('click', function(e) {
      e.preventDefault();
      timesheetsTable.DataTable().ajax.reload();
    });

    $('body').on('change','#group_by_task',function(){
      var tApi = timesheetsTable.DataTable();
      var visible = $(this).prop('checked') == false;
      var tEndTimeIndex = $('.t-end-time').index();
      var tStartTimeIndex = $('.t-start-time').index();
      if(tEndTimeIndex == -1 && tStartTimeIndex == -1) {
        tStartTimeIndex = $(this).attr('data-start-time-index');
        tEndTimeIndex = $(this).attr('data-end-time-index');
      } else {
        $(this).attr('data-start-time-index',tStartTimeIndex);
        $(this).attr('data-end-time-index',tEndTimeIndex);
      }
      tApi.column(tEndTimeIndex).visible(visible, false).columns.adjust();
      tApi.column(tStartTimeIndex).visible(visible, false).columns.adjust();
      tApi.ajax.reload();
    });

    var timesheetsChart;
    var Timesheets_ServerParams = {};
    Timesheets_ServerParams['range'] = '[name="range"]';
    Timesheets_ServerParams['period-from'] = '[name="period-from"]';
    Timesheets_ServerParams['period-to'] = '[name="period-to"]';
    Timesheets_ServerParams['staff_id'] = '[name="staff_id"]';
    Timesheets_ServerParams['project_id'] = 'select#project_id';
    Timesheets_ServerParams['clientid'] = 'select#clientid';
    Timesheets_ServerParams['group_by_task'] = '[name="group_by_task"]:checked';
    initDataTable('.table-timesheets-report', window.location.href, undefined, undefined, Timesheets_ServerParams, [<?php if(isset($view_all)){echo 3;} else {echo 2;} ?>, 'desc']);

    init_ajax_project_search_by_customer_id();

    $('#clientid').on('change', function(){
          var projectAjax = $('select#project_id');
          var clonedProjectsAjaxSearchSelect = projectAjax.html('').clone();
          var projectsWrapper = $('.projects-wrapper');
          projectAjax.selectpicker('destroy').remove();
          projectAjax = clonedProjectsAjaxSearchSelect;
          $('#project_ajax_search_wrapper').append(clonedProjectsAjaxSearchSelect);
          init_ajax_project_search_by_customer_id();
    });
</script>
</body>
</html>
