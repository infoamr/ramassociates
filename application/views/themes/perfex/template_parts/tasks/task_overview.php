<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<div class="nav nav-tabs no-margin">
  <div class="panel-heading project-info-bg no-radius" style="font-weight: 600; font-size: 14px;"><?php echo _l('project_description'); ?></div>
  <div class="panel-body no-radius tc-content project-description">
    <?php 
      if(!empty($task->description)){
        echo '<div class="tc-content"><div id="task_view_description">' .check_for_links($task->description) .'</div></div>';
        }
      else
        {
          echo '<div class="no-margin tc-content task-no-description" id="task_view_description"><span class="text-muted">' . _l('task_no_description') . '</span></div>';
        }
    ?>
  </div>
</div>
<div class="clearfix"></div>
<div class="col-md-12">
  <hr />
</div>
<div class="row mtop15">
 <div class="col-md-6">
  <div class="panel-heading project-info-bg no-radius" style="font-weight: 600; font-size: 14px;"><?php echo _l('task_overview'); ?></div>
  <div class="panel-body no-radius">
  <table class="table table-borded no-margin">
    <tbody>
      <tr>
        <td class="bold"><?php echo _l('task'); ?> <?php echo _l('the_number_sign'); ?></td>
        <td><?php echo $task->task_number; ?></td>
      </tr>
      <?php if($task->settings->view_finance_overview == 1){ ?>
        <tr class="project-billing-type">
          <td class="bold"><?php echo _l('project_billing_type'); ?></td>
          <td>
           <?php
              if($project->billing_type == 1){
                $type_name = 'task_billing_type_fixed_cost';
              } else if($task->billing_type == 2){
                $type_name = 'task_billing_type_project_hours';
              } else {
                $type_name = 'task_billing_type_project_task_hours';
              }
              echo _l($type_name);
          ?>
        </td>
      </tr>
    <?php } ?>
    <?php if(($task->billing_type == 1 || $task->billing_type == 2) && $task->settings->view_finance_overview == 1){
     echo '<tr class="project-cost">';
     if($task->billing_type == 1){
       echo '<td class="bold">'._l('task_total_cost').'</td>';
       echo '<td>'.app_format_money($task->task_cost, $currency).'</td>';
     } else {
       echo '<td class="bold">'._l('project_rate_per_hour').'</td>';
       echo '<td>'.app_format_money($task->task_rate_per_hour, $currency).'</td>';
     }
     echo '<tr>';
   }
   ?>
   
  <tr>
    <td class="bold">Work Type</td>
    <td><?php echo $task->work_type; ?></td>
  </tr>
  
   <tr>
    <td class="bold"><?php echo _l('task_status'); ?></td>
    <td><?php echo $task_status['name']; ?></td>
  </tr>
  <tr>
    <td class="bold"><?php echo _l('task_single_start_date'); ?></td>
    <td><?php echo _d($task->startdate); ?></td>
  </tr>
  <tr>
    <td class="bold"><?php echo _l('task_single_due_date'); ?></td>
    <td><?php echo _d($task->duedate); ?></td>
  </tr>
  <?php if($task->deadline){ ?>
    <tr>
      <td class="bold"><?php echo _l('project_deadline'); ?></td>
      <td><?php echo _d($task->deadline); ?></td>
    </tr>
  <?php } ?>
  <?php if($task->date_finished){ ?>
    <tr class="text-success">
      <td class="bold"><?php echo _l('project_completed_date'); ?></td>
      <td><?php echo _d($task->date_finished); ?></td>
    </tr>
  <?php } ?>
  <?php if($task->billing_type == 1 && $task->settings->view_task_total_logged_time == 1){ ?>
    <tr class="project-total-logged-hours">
      <td class="bold"><?php echo _l('project_overview_total_logged_hours'); ?></td>
      <td><?php echo seconds_to_time_format($this->projects_model->total_logged_time($project->id)); ?></td>
    </tr>
  <?php } ?>
  <?php $custom_fields = get_custom_fields('tasks',array('show_on_client_portal'=>1));
  if(count($custom_fields) > 0){ ?>
    <?php foreach($custom_fields as $field){ ?>
      <?php $value = get_custom_field_value($task->id,$field['id'],'tasks');
      if($value == ''){continue;} ?>
      <tr>
        <td class="bold"><?php echo ucfirst($field['name']); ?></td>
        <td><?php echo $value; ?></td>
      </tr>
    <?php } ?>
  <?php } ?>
</tbody>
</table>
</div>
</div>

<div class="col-md-4 task-single-col-right">
  <div class="task-info task-info-priority">
      <h5 style="font-weight: 600; color:#333!important">
         <i class="fa task-info-icon fa-fw fa-lg pull-left fa-bolt"></i>
         <?php echo _l('task_single_priority'); ?>:
         <span class="task-single-menu task-menu-priority">
            <?php echo task_priority($task->priority); ?>
         </span>
      </h5>
  </div>
   <hr class="task-info-separator" />
   <h5 class="task-info-heading font-normal font-medium-xs" style="font-weight: 600; padding-leftcolor:#333!important">
    <i class="fa fa-users fa-fw pull-left" aria-hidden="true"></i>
    <?php echo _l('task_single_assignees'); ?>
   </h5>

  <div class="task_users_wrapper" style="padding-left: 22px;">
    <?php $_assignees = '';
      if (empty($task->assignees)) {
        $_assignees = '<div class="text-danger display-block">'._l('task_no_assignees').'</div>';
        echo $_assignees;
      }
      else {
        foreach ($task->assignees as $assignee) {
          echo $assignee['full_name'].'<br/>';
         }
      } ?>
  </div>
  <hr class="task-info-separator" />
</div>






  
