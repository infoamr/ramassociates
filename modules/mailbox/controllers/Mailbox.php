<?php

defined('BASEPATH') or exit('No direct script access allowed');

/**
 * Maibox Controller
 */
class Mailbox extends AdminController
{
    /**
     * Controler __construct function to initialize options
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->model('mailbox_model');
        $this->load->model('staff_model');
        $this->load->model('clients_model');
        $this->load->library('session');
    }

    /**
     * Go to Mailbox home page
     * @return view
     */
    public function index()
    {
        $member = $this->staff_model->get(get_staff_user_id());
        $data['member']            = $member;
        $data['office_pwd']        = $member->office_password;
        $data['title'] = _l('mailbox');
        $group         = !$this->input->get('group') ? 'inbox' : $this->input->get('group');
        $data['group'] = $group;

        if(isset($_GET["code"])) 
        {
            $auth_code = $_GET['code'];
            $tokens = $this->getTokenFromAuthCode($auth_code, 'https://newcrm.suninternationalusa.com/livecrm/admin/mailbox');

            if(isset($tokens['access_token']))
            {
                $data['acctoken'] = $tokens['access_token'];
                $this->session->set_userdata('access_token', $tokens['access_token']);
                $this->session->set_userdata('user_email', $tokens['access_token']);
                // Redirect back to home page
                // $this->load->view('mailbox', $data);
            }
        }

        if(!empty($data['office_pwd'])){
            $this->load->view('mailbox', $data);
        }
        else {
            $this->load->view('loginmail', $data);
        }
    }

    public function officepwd()
    {
        if ($this->input->post()) {
            $id = get_staff_user_id();
            $this->db->where('staffid', $id);
            $this->db->update(db_prefix() . 'staff', [
                'office_password' => app_hash_password($this->input->post('office_password')),
            ]);
        }

        $this->load->view('mailbox', $data);
    }

    public static function getLoginUrl($redirectUri) {
      $loginUrl = authority.sprintf(authorizeUrl, CLIENT_ID , urlencode(REDIRECT_URI));
      error_log("Generated login URL: ".$loginUrl);
      return $loginUrl;
    }

    public function getTokenFromAuthCode($authCode, $redirectUri) {
      // Build the form data to post to the OAuth2 token endpoint

      $token_request_data = array(
        "grant_type" => "authorization_code",
        "code" => $authCode,
        "redirect_uri" => $redirectUri,
        "resource" => "https://outlook.office365.com/",
        "client_id" => CLIENT_ID,
        "client_secret" => CLIENT_SECRET
      );
      
      // Calling http_build_query is important to get the data
      // formatted as Azure expects.
      $token_request_body = http_build_query($token_request_data);
      error_log("Request body: ".$token_request_body);
      
      $curl = curl_init(authority.tokenUrl);
      curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
      curl_setopt($curl, CURLOPT_POST, true);
      curl_setopt($curl, CURLOPT_POSTFIELDS, $token_request_body);
      
      $response = curl_exec($curl);
      error_log("curl_exec done.");
      
      $httpCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
      error_log("Request returned status ".$httpCode);
      if ($httpCode >= 400) {
        return array('errorNumber' => $httpCode,
                     'error' => 'Token request returned HTTP error '.$httpCode);
      }
      
      // Check error
      $curl_errno = curl_errno($curl);
      $curl_err = curl_error($curl);
      if ($curl_errno) {
        $msg = $curl_errno.": ".$curl_err;
        error_log("CURL returned an error: ".$msg);
        return array('errorNumber' => $curl_errno,
                     'error' => $msg);
      }
      
      curl_close($curl);
      
      // The response is a JSON payload, so decode it into
      // an array.
      $json_vals = json_decode($response, true);
      error_log("TOKEN RESPONSE:");
      foreach ($json_vals as $key=>$value) {
        error_log("  ".$key.": ".$value);
      }
      
      return $json_vals;
    }

    public function profile() {
        $headers = array(
            "User-Agent: phplift.net/1.0",
            "Authorization: Bearer ".$_SESSION["access_token"],
            "Accept: application/json"
        );
        $outlookApiUrl = api_url."/Me";

        $response = $this->runCurl($outlookApiUrl, null, $headers);
        $response = explode("\n", trim($response));
        $response = $response[count($response) - 1];
        $response = json_decode($response);
        // echo $response; exit;
        $data['usid'] = $response->Id."<br><br>";
        $data['usmail'] = $response->EmailAddress;
        $_SESSION["user_email"] = $response->EmailAddress;

        $this->load->view('calendar', $data);
    }

    public function myinbox() {

        // echo $_SESSION["access_token"]; exit;
        $headers = array(
            "User-Agent: phplift.net/1.0",
            "Authorization: Bearer ".$_SESSION["access_token"],
            "Accept: application/json",
            "X-AnchorMailbox: ".$_SESSION["user_email"]
        );
        $top = 500;
        $skip = isset($_GET["skip"]) ? intval($_GET["skip"]) : 0;
        $search = array (
            // Only return selected fields
            "\$select"=>"Subject,ReceivedDateTime,Sender,From,ToRecipients,HasAttachments,BodyPreview",
            // Sort by ReceivedDateTime, newest first
            "\$orderby"=>"ReceivedDateTime DESC",
            // Return at most n results
            "\$top"=>$top, "\$skip" => $skip
        );
        $outlookApiUrl = api_url . "/Me/MailFolders/Inbox/Messages?" . http_build_query($search);

        $response = $this->runCurl($outlookApiUrl, null, $headers);

        $response = explode("\n", trim($response));
        $response = $response[count($response) - 1];
        $response = json_decode($response, true);

        if(isset($response)) 
        {  
            foreach ($response as $err) {
                $data['errorcode'] = $err['code'];
            }
            if($data['errorcode'] == 'TokenExpired') {
                $this->session->unset_userdata($_SESSION["access_token"]);
            }
        }

        if(isset($response["value"]) && count($response["value"]) > 0) {
            $data['value'] = $response["value"];
        }
        else {
            $data['errmsg'] = 'No email found';
        }
        
        // $lsUrl = admin_url('mailbox/myinbox');
        // $prevLink = "";
        // if($skip > 0) {
        //     $prev = $skip - $top;
        //     $prevLink = '<a style="color:grey; border: 1px solid #03a9f4" class="btn btn-outline-primary" href="'.$lsUrl.'?skip='.$prev.'">Previous Page</a>';
        // }
        // if(isset($response["@odata.nextLink"])) {
        //     if($prevLink != "") {
        //         $prevLink .= " ||| ";
        //     }
        //     $prevLink.= '<a style="color:grey; border: 1px solid #03a9f4" class="btn btn-outline-primary" href="'.$lsUrl.'?skip='.($skip + $top).'">Next Page</a>';
        // }
        // else {
        //     $prevLink = $prevLink;
        // }

        // $data['prevLink'] = $prevLink;
        $this->load->view('mailbox', $data);
    }

    public function sentitems() {

        $headers = array(
            "User-Agent: phplift.net/1.0",
            "Authorization: Bearer ".$_SESSION["access_token"],
            "Accept: application/json",
            "X-AnchorMailbox: Aamir@RamAssociates.us"
        );
        $top = 500;
        $skip = isset($_GET["skip"]) ? intval($_GET["skip"]) : 0;
        $search = array (
            // Only return selected fields
            "\$select" => "Subject,ReceivedDateTime,Sender,From,ToRecipients,HasAttachments,BodyPreview",
            // Sort by ReceivedDateTime, newest first
            "\$orderby" => "ReceivedDateTime DESC",
            // Return at most n results
            "\$top" => $top, "\$skip" => $skip
        );
        $outlookApiUrl = api_url . "/Me/MailFolders/SentItems/Messages?" . http_build_query($search);

        $response = $this->runCurl($outlookApiUrl, null, $headers);

        $response = explode("\n", trim($response));
        $response = $response[count($response) - 1];
        $response = json_decode($response, true);
        // echo "<pre>"; print_r($response); echo "</pre>"; exit;

        if(isset($response["value"]) && count($response["value"]) > 0) {
            $data['value'] = $response["value"];
        }
        else {
            $data['errmsg'] = 'No email found';
        }
        
        // $lsUrl = admin_url('mailbox/myinbox');
        // $prevLink = "";
        // if($skip > 0) {
        //     $prev = $skip - $top;
        //     $prevLink = '<a href="'.$lsUrl.'?skip='.$prev.'">Previous Page</a>';
        // }
        // if(isset($response["@odata.nextLink"])) {
        //     if($prevLink != "") {
        //         $prevLink .= " ||| ";
        //     }
        //     $prevLink.= '<a href="'.$lsUrl.'?skip='.($skip + $top).'">Next Page</a>';
        // }
        // else {
        //     $prevLink = $prevLink;
        // }

        // $data['prevLink'] = $prevLink;
        $this->load->view('mailbox', $data);
    }

    public function drafts() {

        // echo $_SESSION["access_token"]; exit;
        $headers = array(
            "User-Agent: phplift.net/1.0",
            "Authorization: Bearer ".$_SESSION["access_token"],
            "Accept: application/json",
            "X-AnchorMailbox: Aamir@RamAssociates.us"
        );
        $top = 100;
        $skip = isset($_GET["skip"]) ? intval($_GET["skip"]) : 0;
        $search = array (
            // Only return selected fields
            "\$select" => "Subject,ReceivedDateTime,Sender,From,ToRecipients,HasAttachments,BodyPreview",
            // Sort by ReceivedDateTime, newest first
            "\$orderby" => "ReceivedDateTime DESC",
            // Return at most n results
            "\$top" => $top, "\$skip" => $skip
        );
        $outlookApiUrl = api_url . "/Me/MailFolders/Drafts/Messages?" . http_build_query($search);
        $response = $this->runCurl($outlookApiUrl, null, $headers);

        $response = explode("\n", trim($response));
        $response = $response[count($response) - 1];
        $response = json_decode($response, true);
        // echo "<pre>"; print_r($response); echo "</pre>"; exit;

        if(isset($response["value"]) && count($response["value"]) > 0) {
            $data['value'] = $response["value"];
        }
        else {
            $data['errmsg'] = 'No email found';
        }
        
        // $lsUrl = admin_url('mailbox/myinbox');
        // $prevLink = "";
        // if($skip > 0) {
        //     $prev = $skip - $top;
        //     $prevLink = '<a href="'.$lsUrl.'?skip='.$prev.'">Previous Page</a>';
        // }
        // if(isset($response["@odata.nextLink"])) {
        //     if($prevLink != "") {
        //         $prevLink .= " ||| ";
        //     }
        //     $prevLink.= '<a href="'.$lsUrl.'?skip='.($skip + $top).'">Next Page</a>';
        // }
        // else {
        //     $prevLink = $prevLink;
        // }

        // $data['prevLink'] = $prevLink;
        $this->load->view('mailbox', $data);
    }

    public function trash() {

        // echo $_SESSION["access_token"]; exit;
        $headers = array(
            "User-Agent: phplift.net/1.0",
            "Authorization: Bearer ".$_SESSION["access_token"],
            "Accept: application/json",
            "X-AnchorMailbox: Aamir@RamAssociates.us"
        );
        $top = 500;
        $skip = isset($_GET["skip"]) ? intval($_GET["skip"]) : 0;
        $search = array (
            // Only return selected fields
            "\$select" => "Subject,ReceivedDateTime,Sender,From,ToRecipients,HasAttachments,BodyPreview",
            // Sort by ReceivedDateTime, newest first
            "\$orderby" => "ReceivedDateTime DESC",
            // Return at most n results
            "\$top" => $top, "\$skip" => $skip
        );
        $outlookApiUrl=api_url . "/Me/MailFolders/DeletedItems/Messages?" . http_build_query($search);

        $response = $this->runCurl($outlookApiUrl, null, $headers);

        $response = explode("\n", trim($response));
        $response = $response[count($response) - 1];
        $response = json_decode($response, true);
        // echo "<pre>"; print_r($response); echo "</pre>"; exit;

        if(isset($response["value"]) && count($response["value"]) > 0) {
            $data['value'] = $response["value"];
        }
        else {
            $data['errmsg'] = 'No email found';
        }
        
        // $lsUrl = admin_url('mailbox/myinbox');
        // $prevLink = "";
        // if($skip > 0) {
        //     $prev = $skip - $top;
        //     $prevLink = '<a href="'.$lsUrl.'?skip='.$prev.'">Previous Page</a>';
        // }
        // if(isset($response["@odata.nextLink"])) {
        //     if($prevLink != "") {
        //         $prevLink .= " ||| ";
        //     }
        //     $prevLink.= '<a href="'.$lsUrl.'?skip='.($skip + $top).'">Next Page</a>';
        // }
        // else {
        //     $prevLink = $prevLink;
        // }

        // $data['prevLink'] = $prevLink;
        $this->load->view('mailbox', $data);
    }

    public function maild($id) {
         // echo $id; exit;
        $headers = array(
            "User-Agent: phplift.net/1.0",
            "Authorization: Bearer ".$_SESSION["access_token"],
            "Accept: application/json",
            "X-AnchorMailbox:  Aamir@RamAssociates.us"
        );
        $outlookApiUrl = api_url . "/me/Messages('$id')";
        $response = $this->runCurl($outlookApiUrl, null, $headers);
        $response = explode("\n", trim($response));
        $response = $response[count($response) - 1];
        $response = json_decode($response, true);
        // echo '<pre>'; print_r($response); exit;

        $ReceivedDateTime = date("Y-m-d H:i:s",mktime($response['ReceivedDateTime']));

        $data['response'] = $response;
        $data['id'] = $id;
        $this->load->view('maildetails', $data);
    }

    public function runCurl($url, $post = null, $headers = null) {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, $post == null ? 0 : 1);
        if($post != null) {
            curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
        }
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        if($headers != null) {
            curl_setopt($ch, CURLOPT_HEADER, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        }
        $response = curl_exec($ch);
        $http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);
        if($http_code >= 400) {
            //echo "Error executing request to Office365 api with error code=$http_code<br/><br/>\n\n";
            //echo "<pre>"; print_r($response); echo "</pre>";
            //die();
            $data['errorcode'] = $http_code;
            if($http_code == 401)
            {
                $this->session->unset_userdata($_SESSION["access_token"]);
            }
            //$this->load->view('mailbox', $data);
        }
        return $response;
    }

    /**
     * Go to Compose Form
     * @param  integer $outbox_id 
     * @return view
     */

    public function compose($outbox_id = null)
    {
        $headers = array(
            "User-Agent: phplift.net/1.0",
            "Authorization: Bearer ".$_SESSION["access_token"],
            "Accept: application/json",
            "X-AnchorMailbox:  Aamir@RamAssociates.us"
        );

        $data['title'] = _l('mailbox');
        $group         = 'compose';
        $data['group'] = $group;
        if ($this->input->post()) {
            $data  = $this->input->post();  

            if($this->input->post('sendmail')=='draft'){
                // print_r($data); exit;
                // https://outlook.office.com/api/v2.0/me/messages
                // $outlookApiUrl = api_url . "/me/Messages";
                // $response = explode("\n", trim($response));
                // $response = $response[count($response) - 1];
                // $response = json_decode($response, true);
                // $data['response'] = $response;

                // set_alert('success', _l('mailbox_email_draft_successfully', $id));
                redirect(admin_url('mailbox/drafts'));    
            } else {
                // print_r($data); exit;
                // https://outlook.office.com/api/v2.0/me/sendmail
                // $outlookApiUrl = api_url . "/me/sendmail";
                // $response = explode("\n", trim($response));
                // $response = $response[count($response) - 1];
                // $response = json_decode($response, true);
                // $data['response'] = $response;

                // set_alert('success', _l('mailbox_email_sent_successfully', $id));
                redirect(admin_url('mailbox/sentitems'));    
            }
        }

        if(isset($outbox_id)){
            $mail = $this->mailbox_model->get($outbox_id,'outbox');
            $data['mail'] = $mail;
        }
        $data['staff_email'] = $this->clients_model->get_all_staff();
        $this->load->view('mailbox', $data);
    }

    /**
     * Action for reply, reply all and forward
     * @param  integer $id     
     * @param  string $method 
     * @param  string $type   
     * @return view        
     */
    public function reply($id, $method = 'reply',$type = 'inbox'){ 

        // echo $id; exit;
        $headers = array(
            "User-Agent: phplift.net/1.0",
            "Authorization: Bearer ".$_SESSION["access_token"],
            "Accept: application/json",
            "X-AnchorMailbox:  Aamir@RamAssociates.us"
        );
        $outlookApiUrl = api_url . "/me/Messages('$id')";
        $response = $this->runCurl($outlookApiUrl, null, $headers);
        $response = explode("\n", trim($response));
        $response = $response[count($response) - 1];
        $response = json_decode($response, true);
        $ReceivedDateTime = date("Y-m-d H:i:s",mktime($response['ReceivedDateTime']));
        if ($this->input->post()) {
            $postdata = $this->input->post();
            if ($this->input->post('method') == 'reply') {
                // https://outlook.office.com/api/v2.0/me/messages/{message_id}/reply
                // $replyApiUrl = api_url . "/me/Messages('$id')/reply";
                // $replyresp = $this->runCurl($replyApiUrl, null, $headers);
                // $replyresp = explode("\n", trim($replyresp));
                // $replyresp = $replyresp[count($replyresp) - 1];
                // $replyresp = json_decode($replyresp, true);
                //if ($replyresp) {
                    //set_alert('success', _l('mailbox_email_sent_successfully', $id));
                    redirect(admin_url('mailbox/myinbox'));
                //}
            }
            if ($this->input->post('method') == 'forward') {
                //https://outlook.office.com/api/v2.0/me/messages/{message_id}/forward
                // $forwardApiUrl = api_url . "/me/Messages('$id')/forward";
                // $forwardresp = $this->runCurl($forwardApiUrl, null, $headers);
                // $forwardresp = explode("\n", trim($forwardresp));
                // $forwardresp = $forwardresp[count($forwardresp) - 1];
                // $forwardresp = json_decode($forwardresp, true);
                //if ($forwardresp) {
                    //set_alert('success', _l('mailbox_email_sent_successfully', $id));
                    redirect(admin_url('mailbox/myinbox'));
                //}
            }
        } 

        $data['response'] = $response;
        $data['id'] = $id;
        $data['group'] = $group;
        $data['type'] = 'reply';
        $data['action_type'] = $type;
        $data['method'] = $method;
        $data['staff_email'] = $this->clients_model->get_all_staff();
        $this->load->view('mailbox', $data); 
    }

    /**
     * Get list email to dislay on datagrid
     * @param  string $group
     * @return 
     */
    public function table($group = 'inbox'){
        if ($this->input->is_ajax_request()) {
            if($group == 'sent' || $group == 'draft'){
                $this->app->get_table_data(module_views_path('mailbox', 'table_outbox'),[
                    'group' => $group,
                ]);
            } else {
                $this->app->get_table_data(module_views_path('mailbox', 'table'),[
                    'group' => $group,
                ]);
            }
        }
    }

    /**
     * Go to Inbox Page
     * @param  integer $id
     * @return view
     */
    public function inbox($id){
        $inbox = $this->mailbox_model->get($id,'inbox'); 
        $this->mailbox_model->update_field('detail','read',1,$id,'inbox');
        $data['title'] = $inbox->subject;
        $group         = 'detail';
        $data['group'] = $group;   
        $data['inbox'] = $inbox;
        $data['type'] = 'inbox';
        $data['attachments'] = $this->mailbox_model->get_mail_attachment($id,'inbox');
        $this->load->view('mailbox', $data);    
    }

    /**
     * Go to Outbox Page
     * @param  integer $id
     * @return view
     */
    public function outbox($id){
        $inbox = $this->mailbox_model->get($id,'outbox'); 
        $data['title'] = $inbox->subject;
        $group         = 'detail';
        $data['group'] = $group;   
        $data['inbox'] = $inbox;
        $data['type'] = 'outbox';
        $data['attachments'] = $this->mailbox_model->get_mail_attachment($id,'outbox');
        $this->load->view('mailbox', $data);    
    }

    /**
     * update email status
     * @return json
     */
    public function update_field(){
        if ($this->input->post()) {
            $group = $this->input->post('group');
            $action = $this->input->post('action');
            $value = $this->input->post('value');
            $id = $this->input->post('id');
            $type = $this->input->post('type');
            if($action != 'trash'){
                if($value == 1){
                    $value = 0;
                } else {
                    $value = 1;
                }
            }
            $res = $this->mailbox_model->update_field($group,$action,$value,$id,$type);
            $message = _l('mailbox_'.$action).' '._l('mailbox_success');
            if($res == false){
                $message = _l('mailbox_'.$action).' '._l('mailbox_fail');
            }
            echo json_encode([
                'success' => $res,
                'message' => $message,
            ]);

        }

    }

    /**
     * Configure password to receice email from email server
     * @return redirect
     */
    public function config(){
        if ($this->input->post()) {
            $res  = $this->mailbox_model->update_config($this->input->post(),get_staff_user_id());
            if ($res) {
                set_alert('success', _l('mailbox_email_config_successfully'));
                redirect(admin_url('mailbox'));
            }
        }
    }

    public function newmailbox()
    {
        $this->load->view('testmail');
    }

    public function calendar()
    {
        $headers = array(
            "User-Agent: phplift.net/1.0",
            "Authorization: Bearer ".$_SESSION["access_token"],
            "Accept: application/json",
            "X-AnchorMailbox: Aamir@RamAssociates.us"
        );

        $queryParams = array(
          'startDateTime' => '2020-10-14 12:20:26',
          'endDateTime' => '2020-11-14 12:20:26',
          // Only request the properties used by the app
          '$select' => 'subject,organizer,start,end',
          // Sort them by start time
          '$orderby' => 'start/dateTime',
          // Limit results to 25
          '$top' => 25
        );

        $calUrl = api_url . "/me/calendarview?" . http_build_query($queryParams);
        // https://outlook.office.com/api/v2.0/me/calendars/{calendar_id}/calendarview?startDateTime={start_datetime}&endDateTime={end_datetime}

        $response = $this->runCurl($calUrl, null, $headers);

        $response = explode("\n", trim($response));
        $response = $response[count($response) - 1];
        $response = json_decode($response, true);
        // echo "<pre>"; print_r($response); echo "</pre>"; exit;

        if(isset($response["value"]) && count($response["value"]) > 0) {
            $data['value'] = $response["value"];
        }
        else {
            $data['errmsg'] = 'No events found';
        }

        $data['response'] = $response;
        $this->load->view('eventscalendar', $data);
    }

    public function event($id) 
    {
        $headers = array(
            "User-Agent: phplift.net/1.0",
            "Authorization: Bearer ".$_SESSION["access_token"],
            "Accept: application/json",
            "X-AnchorMailbox:  Aamir@RamAssociates.us"
        );
        $outlookApiUrl = api_url . "/me/Messages('$id')";
        $response = $this->runCurl($outlookApiUrl, null, $headers);
        $response = explode("\n", trim($response));
        $response = $response[count($response) - 1];
        $response = json_decode($response, true);

        $data['response'] = $response;
         echo '<pre>'; print_r($response); exit;
        $data['id'] = $id;
        $this->load->view('eventscalendar', $data);
    }

    public function contact()
    {
        $headers = array(
            "User-Agent: phplift.net/1.0",
            "Authorization: Bearer ".$_SESSION["access_token"],
            "Accept: application/json",
            "X-AnchorMailbox: Aamir@RamAssociates.us"
        );

        $queryParams = array(
          // 'startDateTime' => '2020-10-14 12:20:26',
          // 'endDateTime' => '2020-11-14 12:20:26',
          // Only request the properties used by the app
          '$select' => 'EmailAddresses,GivenName,Surname',
          // Sort them by start time
          // '$orderby' => 'start/dateTime',
          // Limit results to 25
          // '$top' => 25
        );

        $contactsQueryParams = array (
          // // Only return givenName, surname, and emailAddresses fields
          "\$select" => "givenName,surname,emailAddresses",
          // Sort by given name
          "\$orderby" => "givenName ASC",
          // Return at most 10 results
          "\$top" => "10"
        );

        $getContactsUrl = api_url .'/me/contacts?'.http_build_query($contactsQueryParams);
        // https://outlook.office.com/api/v2.0/me/tasks
        // $contactUrl = api_url . "/me/contacts?" . http_build_query($queryParams);
        // https://outlook.office.com/api/v2.0/me/contacts?$select=EmailAddresses,GivenName,Surname

        $response = $this->runCurl($getContactsUrl, null, $headers);

        $response = explode("\n", trim($response));
        $response = $response[count($response) - 1];
        $response = json_decode($response, true);
        echo "<pre>"; print_r($response); echo "</pre>"; exit;
    }

}