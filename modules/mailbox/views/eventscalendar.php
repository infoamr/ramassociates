<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<?php init_head(); ?>
<div id="wrapper">
    <div class="content">
        <div class="row">

            <div class="col-md-12">
  
                <div class="panel_s">
                    <div class="panel-body">
                        <div class="tab-content">
                            <h4 class="customer-profile-group-heading">Events</h4>
                            <div class="table-responsive">
                            <table class="table">
                                <thead>
                                  <tr>
                                    <th>Subject</th>
                                    <th>Start</th>
                                    <th>End</th>
                                    <th>Organiser</th>
                                    <th>Action</th>
                                  </tr>
                                </thead>
                                <tbody>
                                  <?php
                                    foreach($value as $eve) 
                                    {
                                        $startdate = date('d-m-Y h:i', strtotime($eve["Start"]["DateTime"]));
                                        $startdate=date("d, F, Y h:i", strtotime($startdate));
                                        $enddate=date('d-m-Y h:i', strtotime($eve["End"]["DateTime"]));
                                        $enddate=date("d, F, Y h:i", strtotime($enddate));
                                        $mUrl=admin_url('mailbox/event/').$eve["Id"];

                                        echo "<tr class='success'>";
                                        echo "<td>".$eve["Subject"]."</td>";
                                        echo "<td>".$startdate."</td>";
                                        echo "<td>".$enddate."</td>";
                                        echo "<td>".$eve["Organizer"]["EmailAddress"]["Name"]."</td>";
                                        echo "<td><a href=".$mUrl.">View</a></td>";
                                        echo "</tr>";
                                    }
                                  ?>
                                </tbody>
                            </table>
                            </div>
                            <div style="float: right">
                                <?php //echo $prevLink; ?>
                            </div>
                        </div>
                    </div>
                </div>
               
            </div>
        </div>
    </div>
</div>
<?php init_tail(); ?>
<script type="text/javascript">
	"use strict";

    $(function(){
        init_btn_with_tooltips();   
        init_tabs_scrollable();   
        var webmailTableNotSortable = [0];
        initDataTable('.table-mailbox', admin_url + 'mailbox/table/<?php echo $group;?>', 'undefined', webmailTableNotSortable, 'undefined', [2, 'desc']);
        appValidateForm($('#mailbox_config_form'), {
           email: 'required',
           mail_password: 'required',
        });
    });
</script>
</body>
</html>