<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<?php echo form_open_multipart($this->uri->uri_string(),array('id'=>'mailbox_compose_form')); ?>
<div class="clearfix mtop20"></div>
<div class="row">
  <div class="col-md-12">
    <div class="form-group">      
    <i class="fa fa-question-circle pull-left" data-toggle="tooltip" data-title="<?php echo _l('mailbox_multi_email_split'); ?>"></i>
    <?php
      $to = '';
      $subject = $response['Subject'];
      $from = $response['Sender']['EmailAddress']['Address'];
      $toArr = array();
      foreach($response['ToRecipients'] as $to)
      {
        $toArr[] = $to['EmailAddress']['Address'];
      } 
      $to = implode(", ",$toArr);

      if($action_type == 'inbox'){
        if($method == 'reply'){
          $subject = 'RE: '.$subject;
          $to = $to;
        } else {
          $subject = 'FW: '.$subject;
        }  
      } else {
        if($method == 'reply'){
          $subject = 'RE: '.$subject;
          $to = $to;
        } else {
          $subject = 'FW: '.$subject;
        }
      }     
    ?>
      <?php echo form_hidden('mail_id', $id); ?>
      <?php echo form_hidden('method', $method); ?>
      <?php if($method == 'forward') { ?>
      <label class="control-label">To</label>
      <select name="to" class="selectpicker" data-live-search="true" data-width="100%">
       <?php foreach($staff_email->result() as $em) { ?>
       <option value="<?php echo $em->email?>"><?php echo $em->email?></option>
       <?php } ?>
      </select>
    <?php } 
    else {
      echo render_input('to','mailbox_to',$from);
    } ?>
    <?php //echo render_input('cc','CC'); ?>
      <label class="control-label">CC</label>
      <select name="cc[]" class="selectpicker" multiple data-live-search="true" data-width="100%">
       <?php foreach($staff_email->result() as $em) { ?>
       <option value="<?php echo $em->email?>"><?php echo $em->email?></option>
       <?php } ?>
      </select>
    <?php echo render_input('subject','mailbox_subject',$subject); ?>
    <hr />    
    <?php echo render_textarea('body','',$mail->body . $mail_signature,array(),array(),'','tinymce tinymce-compose'); ?>    
    </div>
    <div class="attachments">
      <div class="attachment">
        <div class="mbot15">
          <div class="form-group">
            <label for="attachment" class="control-label"><?php echo _l('ticket_add_attachments'); ?></label>
            <div class="input-group">
              <input type="file" extension="<?php echo str_replace('.','',get_option('ticket_attachments_file_extensions')); ?>" filesize="<?php echo file_upload_max_size(); ?>" class="form-control" name="attachments[0]" accept="<?php echo get_ticket_form_accepted_mimes(); ?>">
              <span class="input-group-btn">
                <button class="btn btn-success add_more_attachments p8-half" data-max="<?php echo get_option('maximum_allowed_ticket_attachments'); ?>" type="button"><i class="fa fa-plus"></i></button>
              </span>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="btn-group pull-left">
      <?php $mUrl = admin_url('mailbox/maild/').$id; ?>
      <a href="<?php echo $mUrl;?>" class="btn btn-warning close-send-template-modal"><?php echo _l('cancel'); ?></a>      
    </div>

    <div class="pull-right">            
      <button type="submit" autocomplete="off" data-loading-text="<?php echo _l('wait_text'); ?>" class="btn btn-info">
          <i class="fa fa-paper-plane menu-icon"></i>
          <?php echo _l('mailbox_send'); ?>          
        </button>
    </div>
</div>
</div>
<?php echo form_close(); ?>