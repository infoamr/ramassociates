<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<?php init_head(); ?>
<div id="wrapper" class="customer_profile">
  <div class="content_new">
    <div class="row">
      <div class="col-md-12">
        <div class="panel_s">
          <div class="panel-body">
            <div class="tab-content">
              <h4 class="customer-profile-group-heading">Details</h4>
              <div class="row">
                <div class="additional"></div>
                <div class="col-md-12">
                  <?php //echo '<pre>'; print_r($response);?>
                  <?php echo "From: ".$response['Sender']['EmailAddress']['Name']." (".$response['Sender']['EmailAddress']['Address'].")<br>";
                     echo "To: ";
                     foreach($response['ToRecipients'] as $to)
                     {
                        echo $to['EmailAddress']['Name']." (".$to['EmailAddress']['Name'].") ";
                     }
                      echo "<br>";
                      $ReceivedDateTime = date("Y-m-d H:i:s",mktime($response['ReceivedDateTime']));
                      echo "Received on: ".$ReceivedDateTime."<br>";
                      echo "Subject: ".$response['Subject']."<br><br><hr />";
                      echo $response['Body']['Content'];
                  ?>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div class="btn-bottom-toolbar btn-toolbar-container-out text-right" style="width: 100%;">
        <a class="btn_new btn-info" href="<?php echo admin_url('mailbox/myinbox'); ?>">Back</a>   
        <a href="<?php echo admin_url().'mailbox/reply/'.$id.'';?>" autocomplete="off" data-loading-text="<?php echo _l('wait_text'); ?>" class="btn_new btn-warning">
          <i class="fa fa-reply"> </i>&nbsp;</i><?php echo _l('mailbox_reply'); ?></a>
        <a href="<?php echo admin_url().'mailbox/reply/'.$id.'/forward';?>" autocomplete="off" data-loading-text="<?php echo _l('wait_text'); ?>" class="btn_new btn-info">
          <i class="fa fa-share"> </i>&nbsp;<?php echo _l('mailbox_forward'); ?></a>
      </div>

    </div>
  </div>
</div>

</body>
</html>
